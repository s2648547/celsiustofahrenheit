package nl.utwente.di.CtoF;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TranslatorTest {

	@Test
	public void testTranslation1() throws Exception {
		Converter translator = new Converter();
		double price = translator.convertToF("0");
		Assertions.assertEquals(32.0, price, 0.0, "0 degrees Celsius");
	}

	@Test
	public void testTranslation2() throws Exception {
		Converter translator = new Converter();
		double price = translator.convertToF("100");
		Assertions.assertEquals(212.0, price, 0.0, "100 degrees Celsius");
	}

}
