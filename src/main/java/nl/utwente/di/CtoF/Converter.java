package nl.utwente.di.CtoF;

public class Converter {
	public double convertToF(String s) throws NumberFormatException {
		double celsius = Double.parseDouble(s);
		return celsius * 1.8 + 32;
	}
}
